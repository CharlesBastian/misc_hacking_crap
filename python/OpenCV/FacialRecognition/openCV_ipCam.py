#!/usr/bin/python
import cv2
import sys
import os
import numpy as np

cascPath=sys.argv[1]
faceCascade=cv2.CascadeClassifier(cascPath)

videoCapture=cv2.VideoCapture('http://136.62.222.64:3001/top.htm')
#videoCapture=cv2.VideoCapture()
videoCapture.set(3,640)
videoCapture.set(4,480)
speakCount=0
speaking='no'

while True:
	# Capture frame-by-frame
	ret,frame=videoCapture.read()
	'''
	gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
	
	faces=faceCascade.detectMultiScale(
	  gray,
	  scaleFactor=1.1,
	  minNeighbors=5,
	  minSize=(15,15),
	  flags=cv2.CASCADE_SCALE_IMAGE
	)
	
	# Draw a rectangle around the faces
	for(x,y,w,h) in faces:
		cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)

		if speaking=='no' and speakCount<=0:
			os.system("espeak 'I see you'")
			speaking='yes'
			speakCount=10
		else:
			speakCount-=1
	
	if speakCount<=0:
		speaking='no'
	
	
	# Display the resulting frame
	cv2.imshow('Video',frame)
	
	if cv2.waitKey(1) & 0xFF==ord('q'):
		break
        '''
# When everything is done, release the capture
videoCapture.release()
cv2.destroyAllWindows()
