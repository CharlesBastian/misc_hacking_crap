#!python3
#coding: utf8

import csv
from zlib import compress
#from urllib3 import urlopen
from urllib.request import urlopen

# fetch content of url
sample_csv_url="https://raw.githubusercontent.com/pedroma/python-workshop/master/Sacramentorealestatetransactions.csv"
response=urlopen(sample_csv_url)

# save content into new file locally
f=open("SacramentoRealEstate.csv","w")
f.write(str(response.read()))
f.close()

# read file we just saved
f=open("SacramentoRealEstate.csv","rU")
csv_reader=csv.DictReader(f)

# do some dummy calculations
n_lines=0
n_beds=0
for line in csv_reader:
	n_beds+=int(line["beds"])
	n_lines += 1
f.close()

# and print out result
print("There were {0} in Sacramento's {1} real estate transactions.".format(n_beds, n_lines))

# now that we got what we need let's gzip the file
f=open("SacramentoRealEstate.csv","rb")
gzipped_file_content=compress(f.read())
f.close()

# save the gzipped content into a new file
gzipped_file=open("SacramentoRealEstate.csv.gz","w")
gzipped_file.write(str(gzipped_file_content))
gzipped_file.close()

