#!python3
#coding: utf8

import json

data={'three':3,'five':[1,2,3,4,5],'two':2,'one':1}
print(data)
print(json.dumps(data))

#************************************************************************

json_data='{"one":1,"five":[1,2,3,4,5],"three":3,"two":2}'
print(json_data)
jsonParsed=json.loads(json_data)
print(jsonParsed)
