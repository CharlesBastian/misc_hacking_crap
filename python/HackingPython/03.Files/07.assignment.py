#!python3
#coding: utf8


# 01. [1 points] - How do you open the file used in the previous example in read and write mode?
f=open('./Sacramentorealestatetransactions.csv','rw')


#********************************************************************
# 02. [2 points] - Using the with statement, open a file and print all of its lines one by one:
with open('./file.txt', 'r') as f:
        for line in f:
                print(line)


#********************************************************************
# 03. [1 points] - Define a csv writer that uses the following character as delimiter: !
import csv
with open('./07.assignment.csv','w') as csvFile:
	writer=csv.writer(csvFile,delimiter='!')


#********************************************************************
# 04. [2 points] - Read the data from the file created in the previous section (statistics.json) and print each line with the following prefix: [PROCESSED]
import json
with open('./statistics.json') as f:
	json_object=json.load(f)

for i in json_object:
	print('[PROCESSED] '+str(json_object[i]))


#********************************************************************

