#!python3
#coding: utf8

import requests
from StringIO import StringIO

#*************************************************************

response=requests.get("https://google.com/");
google_content=StringIO(response.content)

#*************************************************************
#Opening and reading from files
#f=open('./file.txt','w');
#f.close()

with open('/home/w3bguy/Development/python/HackingPython/03.Files/file.txt','r') as f:
	read_data=f.read()
	print(' -> '+read_data)


