#!python3
#coding: utf8

#************************************************
#01.Read file with a sentence per line
import csv

data=[]
with open('./Sacramentorealestatetransactions.csv', 'rU') as f:
	reader=csv.reader(f,delimiter=',',dialect='excel')
	for line in reader:
		data.append(line)

#print(data[:1])


#************************************************
#02.Manipulate and gather metrics on each sentence

processed={}
for row in data:
	city=row[1]
	type=row[7]
	if processed.has_key(city):
		#print(city,' - ',type)
		pr_city=processed[city]
		pr_type=pr_city.get(type,[])
		#print(pr_type)
		pr_type.append(row)
		processed[city][type]=pr_type
	else:
		#print('adding: ',city)
		processed[city]={type:[row]}

#print(processed['ANTELOPE'])

def pretty_print_data(data):
	for city in data:
		print("City: %s"%(city,))
		
		for type in data[city]:
			print(" Type: %s - %d"%(type,len(data[city][type])))

pretty_print_data(processed)

#************************************************
#Output a file with the metrics obtained

import json
with open('statistics.json','wb') as f:
	json_data=json.dumps(processed)
	f.write(json_data)


