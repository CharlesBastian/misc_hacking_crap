#!python3
#coding: utf8

#for value in iterable:
#	statement

#**************************************

squared=[]
for n in range(10):
	squared.append(n**2)

print(squared)

#**************************************

squared=[n**2 for n in range(10)]

print(squared)

