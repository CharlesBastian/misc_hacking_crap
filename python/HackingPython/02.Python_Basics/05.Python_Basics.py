#!python3
#coding: utf8

#Classes and object instances
#sample_csv_url="https://raw.githubusercontent.com/pedroma/python-workshop/master/Sacramentorealestatetransactions.csv"
#response=urlopen(sample_csv_url)
#f=open("SacramentoRealEstate.csv","w")
#f.write(response.read())
#f.close()

#*****************************************

class Point1(object):
	x=0
	y=0

p1=Point1()
print(p1.x)
print(p1.y)

#*****************************************

class Point(object):
	x=0
	y=0
	
	def __init__(self,x=0,y=0):
		self.x=x
		self.y=y

p=Point(x=2,y=5)
print(p.x)
print(p.y)

#*****************************************

i=3
print(i.bit_length())

#*****************************************

#articles={}
#articles["article1"]
#Throws error

#*****************************************

#Create class that does not throw an error when accessing a non-existant element
class MyDict(dict):
	def __getitem__(self,key):
		if key not in self:
			return None
		return super(MyDict,selft).__getitem__(key)


articles=MyDict()
print(articles["article"])

#*****************************************

class MyDict(dict):
	def __setitem__(self,key,value):
		if type(value)==int:
			value+=1
		super(MyDict,self).__setitem__(key,value)

articles=MyDict()
articles["art"]=1
print(articles["art"])

#*****************************************
