#!python3
#coding: utf8

#List slices  => Array
numbers=range(10)
print(numbers)
print(numbers[5:])
print(numbers[:5])
print(numbers[len(numbers)-1])
print(numbers[-1])

#**************************************

#Dictionaries  => JSON
articles={
	123:"Article 0",
	124:"Atricle 1"
}
print(articles)
print(articles[123])

print('**********************')

for key in articles:
	print(key,articles[key])

#**************************************

