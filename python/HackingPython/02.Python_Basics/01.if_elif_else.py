#!python3
#coding: utf8

number=4

#if condition:
#	statement
#elif other_condition:
#	statement
#else:
#	statement

#************************************

if number%2==0:
	is_even=True
else:
	is_even=False

print(is_even)

#************************************

is_even=False
if number%2==0:
	is_even=True

print(is_even)

#************************************

is_even=True if number%2==0 else False

print(is_even)
