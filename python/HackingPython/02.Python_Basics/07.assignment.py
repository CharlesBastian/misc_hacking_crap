#!python3
#coding: utf8

#01. Create a list with even numbers from 0 to 50 (1 point).
thisList=[]
i=0
while i<=50:
	if i%2==0:
		thisList.append(i)
	i+=1

print(thisList)

#*************************************************************************
#02. Create the same list, using comprehension lists combined with an if statement (dig a little online if you need) (1 points).
thisList2=[n for n in range(51) if n%2==0]
print(thisList2)

#*************************************************************************
#03. Consider the following dictionaries: d1 = {cheese: parmesan, meat: lamb} and d2={drink: cola, desert: icecream}
## Merge the two dictionaries (1 points)
d1={'cheese':'parmesan','meat':'lamb'}
d2={'drink':'cola','desert':'icecream'}
d1.update(d2)
print(d1)

## Delete the desert from the merged dict d1 (1 points)
del d1['desert']
print(d1)

#*************************************************************************
#04. Assuming you have a this list lst = [0,2,3,4,5,6,7,1,8]:
## Get the element in the middle of the list (1 points):
lst=[0,2,3,4,5,6,7,1,8]
print(lst)
lst=lst[:len(lst)/2]+lst[(len(lst)/2)+1:]
print(lst)

## Append this list [19,32,12] to lst (1 points):
lst.append([19,32,12])
print(lst)

## Remove element 19 and 32 from lst (1 points):
for sL in lst:
	if isinstance(sL,list):
		sL.remove(19)
		sL.remove(32)
print(lst)


#*************************************************************************
#05. Create a new class, called MultiPoint, that receives 4 values in its initialization and creates two Point instances internally (pointa,pointb). 
#    Add a method that returns a Point (sum) with the sum of the pointa and pointb, per coordinate (x+x) and (y+y) (5 points). 
class Point(object):
	def __init__(self):
		self.x=0
		self.y=0

class MultiPoint():
	def __init__(self,a=0,b=0,c=0,d=0):
		self.pointa=Point()
		self.pointb=Point()

	def returnPoint(self):
		return (self.pointa.x+self.pointb.x),(self.pointa.y+self.pointb.y)

testing=MultiPoint(1,2,3,4)
test2=testing.returnPoint()
print(test2)

#*************************************************************************


#*************************************************************************
