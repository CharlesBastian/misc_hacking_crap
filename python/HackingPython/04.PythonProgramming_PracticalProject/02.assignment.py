#!python3
#coding: utf8
import requests,json

#************************************************************************
# 01. [8 points] - Using the requests module, get the json data from this url and save it on a file called events.json 
gitHubData=requests.get('https://api.github.com/events')
ghContent=gitHubData.json()
with open('events.json','w') as f:
       json.dump(ghContent,f)

#************************************************************************
# 02. [5 points] - The lists that we need:
## cities: the list of city names
for item in ghContent:
	print(item)


## temperatures: the list of the temperatures, maintaining the same order of the cities list


## humidities: the list of humidities, maintaining the same order of the cities also


## Create a function that receives the raw json data from the API, processes it and returns a dict with the information in the list above. 
## Create a function that processes the previous dictionary and returns a dictionary that has the city name as key and has the humidity 
## and temperature as values (hint - check the zip function): 
###  {Zlitan: {humidity: 12, temperature: 12}



#************************************************************************


#************************************************************************


#************************************************************************
