#!python3
#coding: utf8

# Read data from an API (http://openweathermap.org)
# Save the raw data in a file for safekeeping
# Transform the data, so that it can be fed to the plot module
# Plot a graph with the weather forecast for the next week

import requests,json
from io import StringIO
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('Agg')
##url='http://api.openweathermap.org/data/2.5/weather?id=5016884&appid=d534842fabd721b3ec797e789856d564'
#url='http://api.openweathermap.org/data/2.5/box/city?bbox=-97.827969,30.161126,-97.632510,30.289665,10&cluster=yes&appid=d534842fabd721b3ec797e789856d564'
url='http://api.openweathermap.org/data/2.5/box/city?bbox=-97.977363,29.776101,-97.313186,30.473322,10&cluster=no&appid=d534842fabd721b3ec797e789856d564'

def getForecast(url):
	# Return the forecast data in json 
	r=requests.get(url)
	io=StringIO()
	return r.json()


#print(getForecast(url))
#with open('01.forecast_20170225.json','w') as f:
#	json.dump(getForecast(url),f)

def processData(data):
	#Return data to be used by the plot lib
	print(data)
	info={
		'cities':[],
		'temperatures':[],
		'humidities':[],
	}
	cities=data['list']
	for city in cities:
		main_data=city['main']
		info['cities'].append(city['name'])
		info['temperatures'].append(main_data['temp'])
		info['humidities'].append(main_data['humidity'])
	return info

def showPlot(data):
	# Compute and plot the bar chart
	cities=tuple(data['cities'])
	temperatures=tuple(data['temperatures'])
	humidities=tuple(data['humidities'])
	N=len(cities)
	# Define the width of each bar, and create a list of positions
	# that will be used to place each bar in the chart
	ind=np.arange(N) # the x locations for the groups
	width=0.35 # the width of the bars
	_,ax=plt.subplots()
	rects1=ax.bar(ind,temperatures,width,color='r')
	rects2=ax.bar(ind+width,humidities,width,color='y')
	
	ax.set_ylabel('Units')
	ax.set_title('Temperature and humidity by city')
	ax.set_xticks((ind+width/2)+0.15)
	ax.set_xticklabels(data['cities'])
	ax.legend((rects1,rects2),('Temperatures','Humidities'))
	
	# Show the bar chart 
	plt.show()


processed=processData(getForecast(url))
print(processed)
showPlot(processed)
